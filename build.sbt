ThisBuild / organization := "perfimmo"
ThisBuild / version      := "0.1.0-SNAPSHOT"
ThisBuild / scalaVersion := "2.12.10"

lazy val backend = (project in file("."))
	.settings(
		name := "testMongoScala",
		libraryDependencies ++= Seq(
			"org.reactivemongo" %% "reactivemongo" % "0.18.7"
			, "org.slf4j" % "slf4j-simple" % "1.7.28"
		)
	)
