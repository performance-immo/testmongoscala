import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.{Cursor, DefaultDB, MongoConnection, MongoDriver}
import reactivemongo.bson.{BSONDocument, document}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future} // use any appropriate context

/*
time mongoimport -d perfimmo -c detailedTickets3 -j 8 --drop export.json
-XX:MaxMetaspaceSize=174m  -XX:+UseParallelGC -XX:+UseParallelOldGC -XX:ParallelGCThreads=2 -XX:MaxGCPauseMillis=1000 -XX:GCTimeRatio=29
JAVA_OPTS="-Xms2G -Xmx12G" time sbt run
 */
object Main extends App {
	println("Yo les amigos !")

	val mongoUri = "mongodb://localhost:27017/mydb?authMode=scram-sha1"

	val driver = MongoDriver()
	val parsedUri = MongoConnection.parseURI(mongoUri)
	val connection = parsedUri.map(driver.connection(_))

	val futureConnection: Future[MongoConnection] = Future.fromTry(connection)
	def db1: Future[DefaultDB] = futureConnection.flatMap(_.database("perfimmo"))

	def ticketsCollection: Future[BSONCollection] = db1.map(_.collection("detailedTickets"))
	def ticketsCopiedCollection: Future[BSONCollection] = db1.map(_.collection("detailedTickets_copiedFromScala"))

	val result = for {
		_       <- ticketsCopiedCollection.flatMap { _.drop(false) }
		tickets <- ticketsCollection.flatMap { _.find(document(), None).cursor[BSONDocument]().collect[List](-1, Cursor.FailOnError[List[BSONDocument]]()) }
		_       <- ticketsCopiedCollection.flatMap { _.insert.many(tickets) }
	} yield ()

	Await.result(result, Duration.Inf)
}